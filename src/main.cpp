#include <iostream>

using namespace std;

int main()
{
    cout << endl;
    cout << "C++ instance module" << endl;
    cout << "Builded " << __DATE__ << " " << __TIME__ << endl;
    cout << endl;

    const int len = 10;
    int my_arr[len] = {0};
    cout << my_arr[10] << endl;

    return 0;
}

#! /bin/sh

# Path to project build script (CMakeLists.txt)
scriptdir=$(pwd)/$(dirname $0)


case "$1" in
    "")
        echo ""
        echo "Configuration menu. Choose build type by command ./config.sh <arg>"
        echo "    d or D - Debug build type"
        echo "    r or R - Release build type";;
    "d" | "D")
        cmake $scriptdir -DCMAKE_BUILD_TYPE=Debug;;
    "r" | "R")
        cmake $scriptdir -DCMAKE_BUILD_TYPE=Release;;
    *)
        echo "Error: Wrong arguments";;
esac

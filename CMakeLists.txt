project(cpp-ins CXX)
cmake_minimum_required(VERSION 3.0)

# Project

## Project header and source files
file(GLOB project_sources src/main.cpp)

## Project target
add_executable(${PROJECT_NAME} ${project_sources})
set_target_properties(${PROJECT_NAME} PROPERTIES CXX_STANDARD 11)
target_compile_definitions(${PROJECT_NAME} PRIVATE MY_DEFINE)
#target_include_directories() no needed
target_compile_options(${PROJECT_NAME} PRIVATE -Wall -Wextra -Wpedantic)
target_compile_options(${PROJECT_NAME} PRIVATE $<$<CONFIG:DEBUG>:-g3 -ggdb3 -O0 -fsanitize=address>)
target_link_libraries(${PROJECT_NAME} debug libasan.a debug -pthread debug -ldl)

install(TARGETS ${PROJECT_NAME} RUNTIME DESTINATION bin)

## Project dependencies
# No deps

## Project tests
# No tests

# Additional commands

## Build this project module with other project modules
if(TARGET dist)
    add_dependencies(dist ${PROJECT_NAME})
endif(TARGET dist)

## Module clean command
add_custom_target(clean-${PROJECT_NAME}
    COMMAND ${CMAKE_MAKE_PROGRAM} clean
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
)
